<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Tag;

class Post extends Model
{
    //caso a sua tabela não seja bem convertida utilize protected $table = 'outratabela'; para apontar para a tabela
    //caso sua chave primaria tenha outro nome utilize protected $primaryKey = 'nomedachave'; para seta-la 
    //para desativar os campos padrões de timestamps do laravel utilize public $timestamps = false; 
    //para trocar o tipo de conexão do banco de dados utilize  protected $connection = 'sqlite';

    //para setar quais campos não podem ser alterados protected $guarded = ['id','created_at','updated_at'];
    use SoftDeletes;

    protected $fillable = ['title','body','published_at'];
    protected $dates = ['created_at', 'updated_at','published_at','deleted_at'];

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('published', function(Builder $builder){
    //         $builder->where('published_at','<',Carbon::now()->format('Y-m-d H:i:s'));
    //     });
    // }

    // public function scopeOfType($query,$type){
    //     return $query->where('type',$type);
    // }
    public function user()
    {
        return $this->belongsTo(User::class);    
    }
    // public function setTitleAttribute($value)
    // {
    //     $this->attributes['title'] = strtolower($value);
    // }
    // public function getTitleAttribute($value){
    //     return ucfirst($value);
    // }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
