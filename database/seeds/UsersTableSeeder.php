<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'Arthur Alves',
            'email'=>'arthur.alvesdeveloper@outlook.com',
            'password'=>bcrypt('minhasenha')
        ]);
    }
}
